class AppDetail {
  var name;
  var sector;
  var developer;
  var year;

  void appName() {
    print(name.toUpperCase());
  }

  AppDetail(
      String appName, String appSector, String appDeveloper, double appYear) {
    this.name = appName;
    this.sector = appSector;
    this.developer = appDeveloper;
    this.year = appYear;
  }
}

void main() {
  var app = AppDetail("SnapScan", "Fintech", "Kobus Ehlers", 2014);

  print(app.name);
  print(app.sector);
  print(app.developer);
  print(app.year);

  app.appName();
}
